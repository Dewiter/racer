﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelRoll : MonoBehaviour
{
    public float        InitJumpSpeed;
    public float        rotateAcceleration;
    public float        rotateDeceleration;
    public float        rotateSpeed;
    public float        minAllowance;
    static public Boost  reference;


    void Start()
    {
        reference = gameObject.GetComponent<Boost>();
    }

    private float       rotation = 360.00f;
    void Update()
    {
        float rotateAmount;
        float curRotate;

        if (rotation < 180)
            rotateAmount =  Mathf.Clamp01(InitJumpSpeed + ((rotation / 180)
                            * rotateAcceleration));
        else
            rotateAmount =  Mathf.Clamp01((((360 - rotation) / 180)
                            * rotateDeceleration));

        if      (reference.CheckPad() &&
                (transform.localEulerAngles.z < minAllowance
                || transform.localEulerAngles.z > 360 - minAllowance))
                rotation = 0;
        
        curRotate = rotateAmount * rotateSpeed * Time.deltaTime;
        rotation += curRotate;
        transform.Rotate(new Vector3(0, 1, 0) * curRotate);

    }

}
