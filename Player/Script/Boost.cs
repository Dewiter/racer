﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boost : MonoBehaviour
{
    public GameObject[]    boost;
    public GameObject      currentPad;

    void Awake()
    {
        boost = GameObject.FindGameObjectsWithTag("BoostPad");
    }

    void OnDrawGizmosSelected()
    {
        Vector3 offset;

        offset = transform.position;
        offset.y -= 1;
        Gizmos.color = new Color(0, 0, 1, .5f);
        Gizmos.DrawCube(offset , new Vector3(1, 1, 1));
    }

    public bool CheckPad()
    {
        Vector3 offset;
        int     i = 0;

        offset      = transform.position;
        offset.y    -= 1;

        while (i < boost.Length)
        {
                if (Vector3.Distance(boost[i].transform.position, offset) <= 2)
                {
                    currentPad = boost[i];
                    return (true);
                }
                else
                    currentPad = null;
            i++;
        }
        return (false);
    }
}

