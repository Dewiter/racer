﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class Mouvement : MonoBehaviour
{
    public float        speed;
    public float        slow;

    public float        max;
    public float        min;
    private Rigidbody   rb;
    static public Boost boost;

    public GameObject   Leftwall;
    public GameObject   Rightwall;
    public GameObject   Anchor;


    void Start()
    {
        rb      = GetComponent<Rigidbody>();
        boost   = GetComponent<Boost>();
    }


    void Update()
    {

        if (Input.GetKey("w") )
        {
            rb.drag = 0;
            if (boost.CheckPad())
            {
                max = 40;
                speed = max;
            }
            else
                max = 15;
            if (speed < max)
                speed   += 0.1f;        // Forward with inertia
            else
                speed = max;
        }
        else
        {
            if (speed > 3)
                speed   -= 0.1f;
            else
                speed = 2;
        }

        if (Input.GetKeyUp(KeyCode.W))      // Slow Down With additional drag
            rb.drag = slow;
        if (Input.GetKey(KeyCode.D))
            CheckWall(1);
        if (Input.GetKey(KeyCode.A))
            CheckWall(-1);
        
        if (Rightwall && !GameObject.Find("AncorPoint"))
            Instantiate(Anchor, Rightwall.transform.position, Rightwall.transform.rotation);

        rb.AddForce(transform.forward * speed); // Adds current speed value to the ship forward
    }

    void CheckWall(int i)
    {
        int layerMask = 1 << 8;
        RaycastHit hit;

        layerMask = ~layerMask;

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right * i)
                            , out hit, Mathf.Infinity, layerMask))
        {
            Debug.DrawRay(transform.position,
                            transform.TransformDirection(Vector3.right * i) * hit.distance,
                            Color.yellow);
            if (i < 0)
                Leftwall = hit.transform.gameObject;
            else if (i > 0)
                Rightwall = hit.transform.gameObject;
            Debug.Log("Hit !");
        }

        else
        {
            Debug.DrawRay(transform.position,
                            transform.TransformDirection(Vector3.right * i) * 1000,
                            Color.white);
            Debug.Log("No hit");
            Leftwall = null;
            Rightwall = null;
        }

    }
}
